# Titlepage package
This is a package that provides a new titlepage that will be generated when
invoking the `\maketitle` command.

You need to define, in addition to the author, date and title, the group you
are in and the supervisor for your course unit :
```latex
    \supervisor{Supervisor goes here}
    \group{Group goes there}
```
To use it, just put the `terrienaleTitle.sty` file on the root of your latex
document, and put in your preambule `\require{terrienaleTitle}`.
The `sample.tex` file shows how it works.
